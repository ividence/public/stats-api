# Ividence Stats API

You will need a PARTNER_ID, API_USER and API_SECRET to access the API. Please request this information from your account manager. 

## Auth

The api uses bearer tokens for accessing resources. Before you can make any queries you must first request an access token.

This is done by making a post request with your API_USER AND API_SECRET:

```
POST /public_api/v2/{PARTNER_ID}/login
Content-Type: application/json

{
    "username":"{API_USER}",
    "secret":"{API_SECRET}"
}
```
In case of success you will receive:
```
{
    "code": 200,
    "token": "TOKEN"
}
```

In case of an error you will receive a similar result with information regarding what went wrong:
```
{
    "code": 401,
    "more_info": "Invalid credentials"
}
```

## Stats
Depending on the access and account type you have there are three different endpoints you can query.

In all cases TOKEN refers to the token obtained from the auth request.

#### Endpoints
##### Ividence for Advertisers:
```
GET /public_api/v2/{PARTNER_ID}/stats/ifa/campaigns
Content-Type: application/json
Authorization: Bearer {TOKEN}
```
##### Ividence for Publishers:
###### Campaign stats
```
GET /public_api/v2/{PARTNER_ID}/stats/ifp/campaigns
Content-Type: application/json
Authorization: Bearer {TOKEN}
```
######  Newsletter stats:
```
GET /public_api/v2/{PARTNER_ID}/stats/ifp/newsletters
Content-Type: application/json
Authorization: Bearer {TOKEN}
```

#### Query parameters
All parameters are optional

###### Specifying a date range for the requested stats:
You can use **start_date=YYYY-MM-DD** and **end_date=YYYY-MM-DD** to request stats for a specific date range

Default values:
- **end_date**: NOW
- **start_date**: **end_date** - 30 days

###### Breaking down stats by different dimensions:
- **breakdown=dim1,dim2,dim3** can be used as a GROUP BY clause, all stats will be broken down by specified dimensions

  Available dimensions: 
   - advertiser
   - campaign
   - ad (alias for creative)
   - creative
   - publisher_domain
   - country
   - date
   - device
   - context (newsletter)
   - slot

  **Note 1**: context & slot are available only for *Ividence for Publishers*
  
  **Note 2**: *Ividence for Publishers newsletter stats* can be grouped only by **context** and **slot**

- **date_interval=day|month** when breaking down stats by date this parameter can be used to specify the interval. Available values are _day_ or _month_. 

  **Known limitation:** stats will be grouped by month when querying for more than 31 days of data

 Example: 

 ```
 GET /public_api/v2/{PARTNER_ID}/stats/ifp/campaigns?start_date=2018-01-01&end_date=2018-03-31&breakdown=campaign,creative,date&date_interval=month
 Content-Type: application/json
 Authorization: Bearer {TOKEN}
 ```
 This will request monthly campaign stats between January and May, grouped by campaign and creative.

 Response:
 ```
 {
   code:200,
   data:[
      {
         date: '2018-01-01',
         campaign_id:'5b1dadad0000000000000000',
         campaign_name:'Test campaign',
         creative_id:'5b1dadad0000000000000001',
         creative_name:'Test creative',
         impressions:111,
         clicks:222,
         conversion:333,
         cost:{
            value:10,
            currency:'EUR'
         }
      },
      {
         date: '2018-02-01',
         campaign_id:'5b1dadad0000000000000000',
         campaign_name:'Test campaign',
         creative_id:'5b1dadad0000000000000001',
         creative_name:'Test creative',
         impressions:1111,
         clicks:2222,
         conversion:3333,
         cost:{
            value:100,
            currency:'EUR'
         }
      },
      {
         date: '2018-03-01',
         campaign_id:'5b1dadad0000000000000000',
         campaign_name:'Test campaign',
         creative_id:'5b1dadad0000000000000001',
         creative_name:'Test creative',
         impressions:1212,
         clicks:2323,
         conversion:3434,
         cost:{
            value:45,
            currency:'EUR'
         }
      }
   ]
}
```

###### Filtering results:

- **campaign_ids**=val1,val2,val3
- **creative_ids**=val1,val2,val3
- **context_ids**=val1,val2,val3 (can only be used by *Ividence for publishers endpoints*)
- **advertiser_ids**=val1,val2,val3
- **advertiser_domains**=domain1.com,domain2.com

 Example: 

 ```
 GET /public_api/v1/{PARTNER_ID}/stats/ifp/campaigns?campaign_id=5b1dadad0000000000000000
 Content-Type: application/json
 Authorization: Bearer {TOKEN}
 ```
 Response:
 ```
 {
   code:200,
   data:[
     {
        impressions:111,
        clicks:222,
        conversion:333,
        cost:{
           value:10,
           currency:'EUR'
        }
     }
   ]
 }
 ```

###### Response model:
In case of success, you will receive the following object:
```
{
   "code":200,
   "data":[STATS_OBJECT1, STATS_OBJECT2, ...]
}
```
Where STATS_OBJECT has the following structure:

Fields will appear depending on the dimensions specified in the **breakdown** parameter
```
{
   date: '2018-05-01',
   advertiser_id: '5b1dadad0000000000000000',
   advertiser_domain: 'domain.com',
   campaign_id: '5b1dadad0000000000000001',
   campaign_name: 'Campaign Test',
   creative_id: '5b1dadad0000000000000002',
   creative_name: 'Creative Test',
   context_id: '5b1dadad0000000000000003',
   context_name: 'Newsletter Test',
   slot: '0',
   country: 'FR',
   countryISO3: 'FRA',
   publisher_domain: 'pubdomain.com',
   device: 'Desktop',
   
   impressions:111,
   clicks:222,
   conversion:333,
   cost:{
      value:10,
      currency:'EUR'
   }
}
```
In case of missing fields or invalid values the API will send back a detailed response. For example, if issuing a query with *breakdown=SOME_INVALID_DIMENSION* the following response will be sent:
```
{
   code:400,
   invalid_params:{
      breakdown:[
         'Invalid breakdown option: SOME_INVALID_DIMENSION'
      ]
   }
}
```