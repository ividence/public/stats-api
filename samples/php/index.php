<?php

const BASE_URL = "<base-url>";
const PARTNER_ID = "<your-partner-id>";
const USERNAME = "<your-api-username>";
const SECRET = "<your-api-secret>";

const LOGIN_URL = BASE_URL . "/public_api/v2/" . PARTNER_ID . "/login";
const IFP_CAMPAIGN_STATS = BASE_URL . "/public_api/v2/" . PARTNER_ID . "/stats/ifp/campaigns";
const IFP_NEWSLETTER_STATS = BASE_URL . "/public_api/v2/" . PARTNER_ID . "/stats/ifp/newsletters";
const IFA_CAMPAIGN_STATS = BASE_URL . "/public_api/v2/" . PARTNER_ID . "/stats/ifa/campaigns";

$token = getToken();

if(!$token) {
    exit();
}

echo "Getting Publisher Campaign Stats\n\n";
getStats(IFP_CAMPAIGN_STATS, $token);
echo "\n";
echo "Getting Publisher Newsletter Stats\n\n";
getStats(IFP_NEWSLETTER_STATS, $token);
echo "\n";
echo "Getting Advertiser Campaign Stats\n\n";
getStats(IFA_CAMPAIGN_STATS, $token);

function getToken()
{
    $ch = curl_init();
    $headers = array('Content-Type: application/json');

    $post_data = array();
    $post_data["username"] = USERNAME;
    $post_data["secret"] = SECRET;

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_URL, LOGIN_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);

    if($result === false) {
        error_log("curl: error getting token: ". $err);
        return null;
    }

    $decoded = json_decode($result);

    if ($decoded->code == 200) {
        return $decoded->token;
    } else {
        error_log("error fetching token: " . $decoded->more_info);
        return null;
    }
}

function getStats($url, $token)
{
    $queryParams = array(
        "start_date" => "2018-05-01",
        "end_date" => "2018-05-31",
        //"breakdown" => "advertiser,campaign,ad,creative,publisher_domain,publisherDomain,country,date,device,context,slot",
        "date_interval" => "month",
        //"date_interval" => "day",
        //"advertiser_domains" => "dom1,dom2,dom3",
        //"advertiser_ids" => "adv1,adv2,adv3",
        //"campaign_ids" => "cmp1,cmp2,cmp3",
        //"creative_ids" => "crea1,crea2,crea3",
        //"context_ids" => "ctx1,ctx2,ctx3",

    );

    $result = curlGet($url, $queryParams, $token);

    if(!$result) {
        return;
    }

    if ($result->code === 400) {
        foreach($result->invalid_params as $k => $val_arr) {
            foreach($val_arr as $v) {
                error_log("param error [". $k. "]: ". $v);
            }
        }
    } else if ($result->code !== 200) {
        error_log("error fetching stats:", print_r($result, true));
    } else {
        $stats = $result->data;

        if(count($stats) == 0) {
            echo "No stats found";
        }

        foreach($stats as $val) {
            var_dump($val);
        }
    }
}

function curlGet($url, $queryParams, $token)
{
    $ch = curl_init();
    $headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $token);

    $url = $url . '?' . http_build_query($queryParams);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);

    if($result === false) {
        error_log("curl: error getting resource: ". $err);
        return null;
    }

    return json_decode($result);
}